# API

The API is pretty bad. It's inconsistent, and tailors only to my needs. Anyway, knock yourself out.


## The server API

### GET `/api/config`

Fetches the current configuration as JSON.

##### Example

```
GET /api/config
```


    {
        "serialPort": "/dev/ttyS2",
        "baudRate": 9600
    }


### POST `/api/config`

Merge configuration into the current one.

##### Example

Before: `{ "baudRate": 9600 }`


    POST /api/config
    { "serialPort": "/dev/ttyS2" }


After: `{ "baudRate": 9600, "serialPort": "/dev/ttyS2" }`


### GET `/api/serialPorts`

Fetches the host's available serial ports.

##### Example


```
GET /api/serialPorts
```

    {
        err: null,
        ports: [
            "/dev/ttyS0",
            "/dev/ttyS1",
            "/dev/ttyS2"
        ]
    }

## Real-time socket.io API

The server only emits one event – `event`.

### Example

    {
        time,                   // UNIX time of the event
        gps: {                  // Info from the GPS
            latitude,           // The latitude at the time of the event
            longitude,          // The longitude at the time of the event
            altitude            // The altitude in meters at the time of the event
        },
        temperature             // Temperature, in celcius,
        pressure                // 
    }