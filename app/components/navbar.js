import React from 'react';
import { Link } from 'react-router';
import { Navbar as BSNavbar, Label, Nav, NavItem, Button } from 'react-bootstrap';
import { LogView } from './logView';
import { connect } from 'react-redux';
import { ConnectionState } from '../actions';

import NavLink from './navLink';
import socket from '../socket';

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            online: false
        }
    }

    componentDidMount() {
    }

    get connectionLabel() {
        switch(this.props.connectionState) {
            case ConnectionState.ONLINE:
                return <Label bsStyle="success">ONLINE</Label>;
            default:
            case ConnectionState.OFFLINE:
                return (<Label bsStyle="danger">OFFLINE</Label>);
        }
    }

    render() {
        var online = this.state.online;

        return (
            <BSNavbar fixedTop={true} fluid={true}>
                <BSNavbar.Header>
                    <BSNavbar.Brand>EnviroSat</BSNavbar.Brand>
                </BSNavbar.Header>
                <Nav pullRight>
                    <NavLink to="/import">Import data</NavLink>
                    <NavLink to="/">Live data</NavLink>
                    <NavLink to="/monitor">Monitor</NavLink>
                    <NavLink to="/settings">Settings</NavLink>
                    <NavItem>{ this.connectionLabel }</NavItem>
                </Nav>

            </BSNavbar>
        )
    }
}

function mapStateToProps(state) {
    return {
        connectionState: state.connectionState
    }
}

export default connect(mapStateToProps)(Navbar);