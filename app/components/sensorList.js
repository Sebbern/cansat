import React from 'react';
import {ListGroup, ListGroupItem, Panel, Col} from 'react-bootstrap';
import SensorGraph from './sensorGraph';

class SensorList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Panel header="Sensors">
                {this.props.sensors.map((sensor, i) => (
                    <Col md={6} sm={12} key={i}>
                        <SensorGraph sensor={sensor} />
                    </Col>
                ))}
            </Panel>
        );
    }
}

export default SensorList;