import React from 'react';
import {
    Grid,
    Row,
    Col,
    Alert,
    Button,
    ButtonGroup,
    Input,
    DropdownButton,
    MenuItem,
    FormControls,
    Modal,
    Glyphicon
} from 'react-bootstrap';

import {
    setSerialPort,
    setBaudRate,
    fetchConfig,
    saveConfig,
    fetchSerialPorts
} from '../actions';

import { connect } from 'react-redux';
import BaudRates from '../../shared/baudRates';

class SettingsFormItem extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <FormControls.Static labelClassName="col-md-2" wrapperClassName="col-md-10" {...this.props}>
            { this.props.children }
        </FormControls.Static>
    }
}


class SettingsView extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onFetchConfig();
    }

    saveConfig() {
        this.props.onSaveConfig();
    }

    render() {
        return (
            <Grid>
                <Row>
                    <h1>
                        <Glyphicon glyph="wrench" />
                        &nbsp;
                        Settings
                    </h1>
                    <hr />
                </Row>
                <Row>
                    <Alert bsStyle="warning">
                        <strong>Warning!</strong> 
                        <p>Changes made here will overwrite<code>cansat.config.json</code> in the <code>cansat</code> folder.</p>
                    </Alert>
                </Row>
                <form className="form-horizontal" onSubmit={this.handleSave}>
                    <Row>
                            <h2 className="col-md-10">
                                <Glyphicon glyph="transfer" />
                                &nbsp;
                                Serial communication
                            </h2>

                            <Button onClick={() => this.props.onFetchSerialPorts() } className="col-md-2">
                                <Glyphicon glyph="refresh" />
                                &nbsp;
                                Refresh ports
                            </Button>
                    </Row>
                    <Row>
                            <hr />
                            <SettingsFormItem label="Port" wrapperClassName="col-md-5">
                                <select
                                    className="form-control"
                                    onChange={ (event) => this.props.onSerialPortChanged(event.target.value) }
                                    value={ this.props.config.get("serialPort") }>

                                    { this.props.serialPorts.map((port) => (
                                        <option key={port} value={port}>{port}</option>
                                    )) }
                                </select>
                            </SettingsFormItem>
                            <SettingsFormItem label="Baud rate" wrapperClassName="col-md-5">
                                <select
                                    className="form-control"
                                    value={this.props.config.get("baudRate")}
                                    onChange={ (event) => this.props.onBaudRateChanged(event.target.value) }>
                                    { BaudRates.map((rate) => (
                                        <option key={rate} value={rate}>{rate}</option>
                                    )) }
                                </select>
                            </SettingsFormItem>
                    </Row>

                    <Row>
                        <hr />
                        <Col md={5} mdPush={3}>
                            <Button
                                bsStyle="primary"
                                bsSize="large"
                                block={true}
                                onClick={this.saveConfig.bind(this)}>
                                
                                Save settings
                            </Button>
                        </Col>
                    </Row>
                </form>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    return {
        settings: state.settings,
        config: state.settings.get("config"),
        serialPorts: state.serialPorts.get("ports")
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSerialPortChanged: (port) => dispatch(setSerialPort(port)),
        onBaudRateChanged: (rate) => dispatch(setBaudRate(rate)),
        onFetchConfig: () => dispatch(fetchConfig()),
        onSaveConfig: () => dispatch(saveConfig()),
        onFetchSerialPorts: () => dispatch(fetchSerialPorts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsView);