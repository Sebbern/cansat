import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import App from './app';
import MainView from './mainView';
import SettingsView from './settingsView';
import ImportView from './importView';
import SerialMonitor from './serialMonitor';
import createBrowserHistory from 'history/lib/createBrowserHistory';

class Routes extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router history={ createBrowserHistory() }>
                <Route path="/" component={App}>
                    <IndexRoute component={MainView} />
                    <Route path="/monitor" component={SerialMonitor} />
                    <Route path="/settings" component={SettingsView} />
                    <Route path="/import" component={ImportView} />
                </Route>
            </Router>
        );
    }
}

export default Routes;
