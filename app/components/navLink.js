import React from 'react';
import { Link } from 'react-router';

class NavLink extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <li>
                <Link to={this.props.to}>
                    { this.props.children }
                </Link>
            </li>
        )
    }
}

export default NavLink;