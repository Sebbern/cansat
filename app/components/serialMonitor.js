import React from 'react';
import _ from 'lodash';
import socket, { eventStream } from '../socket';
import { Col } from 'react-bootstrap';
import { Observable } from 'rx';
import { connect } from 'react-redux';

class SerialMonitor extends React.Component
{
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (<pre style={{
            overflow: "scroll",
            minHeight: "500px"
        }}>
            {this.props.serialLog}
        </pre>);
    }
}

function mapStateToProps(state) {
    return {
        serialLog: state.serialLog
    }
}

export default connect(mapStateToProps)(SerialMonitor);