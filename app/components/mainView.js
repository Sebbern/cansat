import React, { PropTypes } from 'react';
import Globe from './globe';
import SensorList from './sensorList';
import { Grid, Col, Panel, Label, Row, Button, MenuItem } from 'react-bootstrap';
import Navbar from './navbar';
import { eventStream } from '../socket';

class MainView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showGlobe: false
        };
    }

    componentDidMount() {

    }

    toggleGlobe(event, key) {
        this.setState({
            showGlobe: !this.state.showGlobe
        });
    }

    render() {

        let globeComponent = this.state.showGlobe ? (<Globe />) : (<div />);

        return (
            <div>
                <Grid fluid={true}>
                    <Row>
                        <Col md={12}>
                            <Button
                                id="layoutDropdown" bsStyle="link" bsSize="small" md={12}
                                onClick={() => this.toggleGlobe()}>
                                {this.state.showGlobe?"Hide globe":"Show globe"}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            {globeComponent}
                        </Col>
                        <Col md={12}>
                            <SensorList sensors={[
                                'Temperature',
                                'Pressure',
                                'CO2 PPM',
                                'Altitude',
                                'UV Wavelength'
                            ]} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default MainView;