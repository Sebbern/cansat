import React from 'react';
import { Grid, Col } from 'react-bootstrap';

import Navbar from './navbar';
import LogView from './logView';

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Navbar />
                { this.props.children }
            </div>
        );
    }
}

export default App;
