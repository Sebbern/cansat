import React from 'react';
import _ from 'lodash';
import socket, { eventStream } from '../socket';
import { Col } from 'react-bootstrap';
import { Observable } from 'rx';
import Highcharts from 'highcharts/highstock';
import { connect } from 'react-redux';

class SensorGraph extends React.Component {
    constructor(props) {
        super(props);
    }

    applyData(newData) {
        let sensor = this.props.sensor;

        // TODO: Remove ugly HACK
        let isAltitude = false;
        if(sensor == "Altitude") {
            sensor = "Pressure";
            isAltitude = true;
        }

        let data = newData
            .toJS()
            .filter(event => {
                let val = event[sensor];
                return val != null && !isNaN(val)
                    && event.time != null && !isNaN(event.time);
            })
            .map(event => {
                return [
                    event.time,
                    event[sensor]
                ]
            })
            .map(event => {
                if(!isAltitude) return event;

                return [
                    event[0],
                    Math.max(Math.log((event[1]/100)/0.9899)*-7*1000, 0)
                ];
            })

        this.chart.series[0].setData(data, true, true, true);

    }

    componentWillReceiveProps(nextProps) {
        this.applyData(nextProps.data);
    }

    componentDidMount() {
        const chart = this.chart = new Highcharts.StockChart(this.refs.chart, {
            chart: {
                type: "spline",
                height: 240
            },
            title: {
                text: this.props.sensor
            },
            exporting: {
                enabled: false
            },
            rangeSelector: {
                enabled:false
            },
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            tooltip: {
                xDateFormat: "%H:%M:%S"
            },
            yAxis: {
                minRange: 5
            },
            series: [{
                name: this.props.sensor,
                data: []
            }]
        });

        let sensor = this.props.sensor;

        this.applyData(this.props.data);

        /*
        eventStream
            .filter(event => {
                let val = event[sensor];
                return
                val != null && !isNaN(val);
            })
            .map(event => {
                return [
                    event.time,
                    event[sensor]
                ]
            })
            .subscribe(event => {
                let removeOldPoints = chart.series[0].data.length > this.props.maxNumSamples;
                chart.series[0].addPoint(event, true, removeOldPoints);
            });*/
    }

    render() {
        return (<div ref="chart" />);
    }
}

SensorGraph.defaultProps = {
    maxNumSamples: 10,
}

function mapStateToProps(state) {
    return {
        data: state.sensorData
    };
}

export default connect(mapStateToProps)(SensorGraph);