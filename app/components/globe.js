import React from 'react';
import { Col, Panel } from 'react-bootstrap';
import { eventStream } from '../socket';
import _ from 'lodash';

var Globe = React.createClass({
    componentDidMount: function() {
        var viewer = new Cesium.Viewer(this.refs.cesiumContainer, {
            homeButton: false,
            fullscreenButton: false,
            animation: false,
            selectionIndicator: false,
            //baseLayerPicker: false,
            timeline: false,
            search: false,
            baseLayerPicker: true,
            /*terrainProvider: new Cesium.CesiumTerrainProvider({
                url: '//assets.agi.com/stk-terrain/world'
            })*/
        });

        let positionProperty = new Cesium.SampledPositionProperty();
        positionProperty.setInterpolationOptions({
            interpolationAlgorithm: Cesium.HermitePolynomialApproximation,
            interpolationDegree: 4
        });

        this.entity = viewer.entities.add(new Cesium.Entity({
            position: positionProperty,
            path: new Cesium.PathGraphics({
                resolution: 0.25,
                width: 5,
                material: new Cesium.PolylineGlowMaterialProperty({
                    glowPower: 1,
                    color: Cesium.Color.DEEPPINK
                })
            })
        }));

        eventStream
            .map(event => {
                console.log(event);
                return event;
            })
            .filter(event => (
                event.gps
                && event.gps.longitude
                && event.gps.latitude
                && event.gps.altitude
            ))
            .map(event => {
                return {
                    date: Cesium.JulianDate.fromDate(new Date(event.time)),
                    position: Cesium.Cartesian3.fromDegrees(
                        event.gps.longitude,
                        event.gps.latitude,
                        event.gps.altitude
                    )
                }
            })
            .subscribe(event => {
                positionProperty.addSample(event.date, event.position);
            });
    },

    render: function() {
        return (
            <div ref="cesiumContainer" className="globe" />
        );
    }
})

module.exports = Globe;
