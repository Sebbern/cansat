import React from 'react';
import {
    Grid,
    Row,
    Col,
    Alert,
    Button,
    ButtonGroup,
    Input,
    DropdownButton,
    MenuItem,
    FormControls,
    Modal,
    Jumbotron,
    Glyphicon
} from 'react-bootstrap';

import * as API from '../api';

function cancelEvent(e) {
    e.stopPropagation();
    e.preventDefault();
}

export default class ImportModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            status: "ready"
        }
    }

    resetDrag() {
        this.setState({
            status: "ready"
        })
    }

    componentDidMount() {
        const dropzone = this.refs.dropzone;

        dropzone.addEventListener("dragenter", (e) => {
            cancelEvent(e);
            this.setState({
                status: "hovering"
            })
        });

        dropzone.addEventListener("dragover", (e) => {
            cancelEvent(e);
            this.resetDrag();
        });

        dropzone.addEventListener("drop", (e) => {
            cancelEvent(e);
            this.setState({
                status: "working"
            });

            let files = e.dataTransfer.files;

            if(files.length < 1) {
                console.error("Not enough files...");
                this.resetDrag();
            }

            let file = files[0];

            API.replay(file)
                .then(() => {
                    this.setState({
                        status: "done"
                    })
                })
        });
    }

    get message() {
        switch(this.state.status) {
            default:
            case "ready": return "Drop it like it's hot";
            case "hovering": return "C'mon, yeah...";
            case "working": return "Awww yeah, that's it!";
            case "done": return "Nice one!";
        }
    }

    render() {
        return (
            <div style={{
                backgroundColor: "#eee",
                margin: "10px",
                padding: "120px",
                borderRadius: 5
            }} ref="dropzone">
                <h1 style={{
                    textAlign: "center"
                }}>
                    {this.message}
                </h1>
            </div>
        )
    }
}