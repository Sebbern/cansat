import React, { PropTypes } from 'react';
import { Button, Modal } from 'react-bootstrap';

class WarningModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Modal {...this.props} show={this.props.show}>
            <Modal.Header>
                <Modal.Title>{ this.props.title }</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                { this.props.children }
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={this.props.onClose.bind(this)}>Cancel</Button>
                <Button bsStyle="primary" onClick={this.props.onConfirm.bind(this)}>Confirm</Button>
            </Modal.Footer>
        </Modal>
    }
}

WarningModal.propTypes = {
    title: PropTypes.string,
    show: PropTypes.bool,
    onConfirm: PropTypes.func
}

WarningModal.defaultProps = {
    show: false,
    title: "Are you sure?",
    onConfirm: () => {},
    onClose: () => {}
}

export default WarningModal;