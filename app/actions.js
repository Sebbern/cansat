import { createAction } from 'redux-actions';

import * as API from './api';

export const SET_SERIAL_PORT = "SET_SERIAL_PORT";
export const setSerialPort = createAction(SET_SERIAL_PORT);

export const SET_BAUD_RATE = "SET_BAUD_RATE";
export const setBaudRate = createAction(SET_BAUD_RATE);

export const REQUEST_CONFIG = "REQUEST_CONFIG";
export const requestConfig = createAction(REQUEST_CONFIG);

export const RECEIVE_CONFIG = "RECEIVE_CONFIG";
export const receiveConfig = createAction(RECEIVE_CONFIG);

export const RECEIVE_SERIAL_PORTS = "RECEIVE_SERIAL_PORTS";
export const receiveSerialPorts = createAction(RECEIVE_SERIAL_PORTS);

export const REQUEST_SERIAL_PORTS = "REQUEST_SERIAL_PORTS";
export const requestSerialPorts = createAction(REQUEST_SERIAL_PORTS);

export const CONNECTION_STATE_CHANGED = "CONNECTION_STATE_CHANGED";
export const _connectionStateChanged = createAction(CONNECTION_STATE_CHANGED);
export function connectionStateChanged(connectionState) {
    return function(dispatch, getState) {
        dispatch(_connectionStateChanged(connectionState));
    }
}

export const RECEIVE_SENSOR_DATA = "RECEIVE_SENSOR_DATA";
export const receiveSensorData = createAction(RECEIVE_SENSOR_DATA);

export const RECEIVE_SERIAL_LOG = "RECEIVE_SERIAL_LOG";
export const receiveSerialLog = createAction(RECEIVE_SERIAL_LOG);

export const ConnectionState = {
    ONLINE: "ONLINE",
    OFFLINE: "OFFLINE"
}

function shouldFetchSerialPorts(state) {
    return !state.serialPorts.hasFetched;
}

export function fetchSerialPorts() {
    return function(dispatch, getState) {
        dispatch(requestSerialPorts());

        let state = getState();
        return API.serialPorts.get().then(
            (response) => {
                dispatch(receiveSerialPorts(response));
                return response;
            },
            (reason) => dispatch(receiveSerialPorts(new Error(reason)))
        );
    }
}

export function fetchSerialPortsIfNeeded() {
    return function(dispatch, getState) {
        if(shouldFetchSerialPorts(getState())) {
            return dispatch(fetchSerialPorts());
        }
        else {
            return Promise.resolve();
        }
    }
}

export function fetchConfig() {
    return (dispatch) => {
        dispatch(requestConfig());

        dispatch(fetchSerialPortsIfNeeded())
            .then(() => API.config.get())
            .then(
                (config) => {
                    dispatch(receiveConfig(config))
                },
                (reason) => {
                    dispatch(receiveConfig(new Error(reason)))
                }
            )
    }
}

export function saveConfig() {
    return (dispatch, getState) => {
        let state = getState();

        API.config.set(state.settings.get("config").toJS());
    }
}