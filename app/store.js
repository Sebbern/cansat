import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';

const createStoreWithMiddleware = applyMiddleware(
    thunkMiddleware
    //, createLogger()
)(createStore);

const store = createStoreWithMiddleware(rootReducer);

export default store;