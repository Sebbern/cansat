import React from 'react';
import ReactDOM from 'react-dom';

require("react-tap-event-plugin")();

require("normalize.css");
require("bootstrap/dist/css/bootstrap.css");
require("./sass/style.scss");

import Routes from './components/routes';

import store from './store';
import { Provider } from 'react-redux';

window.addEventListener('load', function() {
	Cesium.BingMapsApi.defaultKey = "AlPO_a4eN1K3K7XDxJi6wVNTM0Ecl4gzhwJdodZleuio1XAClWJhm4Acut0ok_-H";

	ReactDOM.render(
        <Provider store={store}>
            <Routes />
        </Provider>,

		document.getElementById("main")
	)
})