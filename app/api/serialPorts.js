import { request } from './util';

const serialPorts = {
    get: _ => request("/api/serialPorts")
}

export default serialPorts;