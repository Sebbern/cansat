import { request } from './util';

const config = {
    get: () => request("/api/config"),
    
    set: config => request("/api/config", {
        method: "POST",
        body: JSON.stringify(config)
    })
};

export default config;