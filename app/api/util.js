import _ from 'lodash';
import fetch from 'isomorphic-fetch';

const DefaultParams = {
    method: "GET",
    headers: {
        "Content-Type": "application/json"
    }
};

function request(url, params) {
    return fetch(url, _.assign({}, DefaultParams, params))
        .then(response => response.json());
}

export { request };