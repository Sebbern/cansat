import { request } from './util';

export default function replay(data) {
    return request("/api/replay", {
        method: "POST",
        body: data,
        headers: {
            "Content-Type": "application/octet-stream"
        }
    });
};