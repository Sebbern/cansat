import Rx from 'rx';
import {
	connectionStateChanged,
	ConnectionState,
	receiveSerialLog,
	receiveSensorData
} from './actions';

import store from './store';

// An io.js singleton
const socket = io();
export default socket;

socket.on('connect',    () => store.dispatch( connectionStateChanged( ConnectionState.ONLINE )));
socket.on('disconnect', () => store.dispatch( connectionStateChanged( ConnectionState.OFFLINE )));

const eventStream = new Rx.Subject();
export { eventStream };

socket.on('event', data => {
	store.dispatch(receiveSensorData(data));
	eventStream.onNext(data);
});

socket.on('serial', data => {
    store.dispatch(receiveSerialLog(data));
})