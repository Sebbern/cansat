import _ from 'lodash';
import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import { Map, List } from 'immutable';
import {
    SET_SERIAL_PORT,
    SET_BAUD_RATE,
    REQUEST_CONFIG,
    RECEIVE_CONFIG,
    CONNECTION_STATE_CHANGED,
    ConnectionState,
    RECEIVE_SERIAL_LOG,
    RECEIVE_SENSOR_DATA
} from './actions';


export const settings = handleActions({
    SET_SERIAL_PORT: (state, action) => state.setIn(['config', 'serialPort'], action.payload),
    SET_BAUD_RATE: (state, action) => state.setIn(['config', 'baudRate'], action.payload),
    REQUEST_CONFIG: (state, action) => state.set('isFetching', true),
    RECEIVE_CONFIG: (state, action) => {
        let newState = state.set('isFetching', false);
        if(action.error) {
            return newState;
        }
        return newState.mergeIn(['config'], action.payload);
    }

}, new Map({
    isFetching: false,
    config: new Map({
        serialPort: null,
        baudRate: 9600
    })
}));



export const serialPorts = handleActions({
    REQUEST_SERIAL_PORTS: (state, action) => state.set('hasFetched', false),
    RECEIVE_SERIAL_PORTS: (state, action) => {
        if(action.error) {
            return state;
        }

        return state.set('hasFetched', true).set('ports', action.payload.ports);
    }

}, new Map({
    hasFetched: false,
    ports: []
}));


/*
    A list of sensor readings from the satellite, in the following format:
    {
        time,
        gps: {
            latitude,
            longitude,
            altitude
        },
        acceleration: {
            x,
            y,
            z
        },
        temperature,
        humidity,
        pressure
    }
*/
export const sensorData = handleActions({
    RECEIVE_SENSOR_DATA: (state, action) => {
        if(action.error) {
            return state;
        }

        return state.push(action.payload);
    }
}, new List());

export const serialLog = handleActions({
    RECEIVE_SERIAL_LOG: (state, action) => {
        if(action.error) {
            return state;
        }

        return state + action.payload + "\n";
    }
}, "");


export const connectionState = handleActions({
    CONNECTION_STATE_CHANGED: (state, action) => action.payload
}, ConnectionState.OFFLINE);


const rootReducer = combineReducers({
    serialPorts,
    settings,
    connectionState,
    sensorData,
    serialLog
});

export default rootReducer;