var path = require("path");

module.exports = {
	devtool: "source-map",
	context: path.join(__dirname, "app"),
	entry: "./entry.js",
	output: {
		path: __dirname + "/dist",
		filename: "bundle.js",
		sourcePrefix: ""
	},
	module: {
		//unknownContextCritical: false,
		loaders: [
			{
				test: /\.js$/,
				include: [ path.join(__dirname, "app") ],
				loader: "babel-loader",
				query: {
					presets: ['es2015', 'react']
				}
			},
			{
				test: /\.(sass|scss)$/,
				include: [ path.join(__dirname, "app/sass") ],
				loader: "style!css!sass"
			},
			{
				test: /\.css$/,
				loader: "style!css"
			},
			{
                test: /\.(png|gif|jpg|jpeg)$/,
                loader: 'file-loader'
            },
            {
            	test: /\.(eot|woff2|woff|ttf|svg)$/,
            	loader: 'url-loader'
            },
            {
            	test: /\.json$/,
            	loader: 'file-loader'
            }
		]
	},
    resolve: {
        shared: "./shared"
    },
	sassLoader: {
    	sourceMap: null
  	}
};