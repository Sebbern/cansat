var server = require("./server");
var io = module.exports = require("socket.io")(server);

var sockets = [];

io.on('connection', function(socket) {
    socket.on('disconnect', () => {
        console.log("disconnection :(");
    });
});