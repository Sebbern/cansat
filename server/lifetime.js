"use strict";

process.stdin.resume();

var exitHandlers = [];

function exitHandler(options, err) {
    if (options.cleanup) {
        console.log('Cleaning up...');
        for (let handler of exitHandlers) {
            handler();
        }
    }
    if (err) {
        console.err("Unhandled exception ${err}")
        console.err(err.stack);
    }
    if (options.exit) {
        process.exit();
    }
}

//when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

//catches uncaught exceptions
//process.on('uncaughtException', exitHandler.bind(null, { exit: true }));

function onBeforeExit(fn) {
    exitHandlers.push(fn);
}

exports.default = onBeforeExit;