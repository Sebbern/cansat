"use strict";

const _ = require("lodash");


const keys = [
    'magic',
    'time',
    'latitude',
    'longitude',
    'altitude',
    //'derived_altitude',
    'Temperature',
    'Pressure',
    'CO2 PPM',
    'co2_r0',
    'UV Wavelength'
];

function parse(data) {
    // Data received from the serial port should be in the following format:
    //      value0,value1,value2,...,valueN-1
    //
    //console.log(data);

    let values = _.map(_.split(data, ','), _.toNumber);
    let result = _.zipObject(keys, values);

    //console.log(result);

    return {
        raw: data,
        parsed: result
    };
}

module.exports = {
    parse,
    keys
};
