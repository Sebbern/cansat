var express = require("express");
var api = express.Router();
var config = require("./config");
var serialport = require("serialport");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var io = require("./socket");
var serialParser = require("./serialParser");

api.use(morgan("dev"));


api.use(bodyParser.json({
    type: "*/json"
}));

api.use(bodyParser.raw({
    type: "application/octet-stream",
    limit: "50mb"
}));

api.get("/serialPorts", function(req, res) {
    serialport.list(function(err, ports) {
        if(err) {
            res.json({
                err: err,
                ports: null
            });
            res.end();
            return;
        }

        res.json({
            err: null,
            ports: ports.map(port => port.comName)
        });
        res.end();
    });
});

api.get("/config", (req, res) => {
    res.json(config.get());
});

api.post("/config", (req, res) => {
    console.log(req.body);
    config.set(req.body);
    res.end();
});

api.post("/events", function(req, res) {
    console.log("BODY: ", req.body);
    var events = req.body;
    req.body.forEach(function(event) {
        io.emit('event', event);
    })
    res.end();
});


api.post("/replay", (req, res) => {
    console.log(req.body);
    req.body
        .toString("ascii")
        .split("\r")
        .map(l => {
            console.log(l);
            return serialParser.parse(l).parsed;
        })
        .forEach(event => {
            //setTimeout(() => {
                io.emit("event", event)
            //}, 3000);
        })
});

module.exports = api;