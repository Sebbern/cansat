"use strict";

let serialport = require("serialport");
let Rx = require("rx");
let serialParser = require("./serialParser");
let config = require("./config");

let serial = null;

function init(port, baudrate) {

    try {
        serial = new serialport.SerialPort(port, {
            baudrate,
            parser: serialport.parsers.readline("\n")
        }, false);

        serial.open((err) => {
            if(err) {
                console.error("Failed to open serial port!");
                console.error(err);
                return;
            }

            console.log("Successfully opened serial port");

            let subject = new Rx.Subject();
            //let observable = Rx.Observable.fromEvent(serial, "data")
            //    .map(serialParser.parse);

            serial.on("data", (data) => {
                subject.onNext(serialParser.parse(data));
            });

            portOpenedHandlers.map((handler) => {
                handler(serial, subject);
            });
        });
    } catch(ex) {
        console.error(`Could not init serial port: No such port ${port}`);
    }
}

function get() {
    return serial;
}

let portOpenedHandlers = [];

function onPortOpened(fn) {
    portOpenedHandlers.push(fn);
}

config.on("changed", function(c) {
    if(serial && serial.isOpen())
        serial.close();
    init(c.get("serialPort"), c.get("baudRate"));
});

module.exports = {
    init: init,
    get: get,
    onPortOpened: onPortOpened
};
