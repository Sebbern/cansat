var onBeforeExit = require("./lifetime");

var express = require("express");
var app = require("./app");
var server = require("./server");
var io = require("./socket");
var fs = require("fs");
var serial = require("./serial");
var api = require("./api");
var config = require('./config');
var rx = require("rx");
var open = require("open");
var _ = require("lodash");

var events = [];

app.use(express.static('app/static'));
app.use(express.static('dist'));
app.use('/cesium', express.static('node_modules/cesium/Build/Cesium'));
app.use('/misc', express.static('misc'));
app.use('/bootstrap', express.static('node_modules/bootstrap/dist'));

app.use("/api", api);

var indexHtml = "refresh the page mayne";
fs.readFile("app/static/index.html", (err, data) => {
    indexHtml = data;
});

app.get("*", function(req, res) {
    res.type("text/html");
    res.end(indexHtml);
});


serial.onPortOpened((port, stream) => {
    stream.subscribe(event => {
        io.emit("event", event.parsed);
        io.emit("serial", event.raw);
    });
});

if(typeof config.get("serialPort") === "string") {
    serial.init(config.get("serialPort"), config.get("baudRate"));
}

server.listen(3000, function() {
    console.log("Listening on :3000");
	if(_.includes(process.argv, "--open-browser"))
		open("http://localhost:3000");
});