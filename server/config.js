"use strict";

var path = require('path');
var fs = require('fs');
var _ = require("lodash");
var Immutable = require("immutable");
var EventEmitter = require("events");

const ConfigPath = exports.configPath = path.join(__dirname, '../cansat.config.json');

const emitter = new EventEmitter();

let defaults = new Immutable.Map({
    baudRate: 9600
});

let globalConfig = new Immutable.Map(defaults);

let normalize = function(config) {
    let result = new Immutable.Map(config);

    let baudRate = result.get("baudRate");

    if(typeof baudRate == "string") {
        result = result.set("baudRate", _.parseInt(baudRate));
    }

    return result;
}

const load = exports.load = function(fn) {
    fs.readFile(ConfigPath, function(err, data) {
        if(err) throw err;

        let newConfig = JSON.parse(data);
        globalConfig = set(newConfig, false);

        console.log("Loaded config:", globalConfig);

        if(fn)
            fn(globalConfig);
    })
}

const save = exports.save = function() {
    console.log("Saving config", globalConfig);

    fs.writeFile(
        ConfigPath,
        JSON.stringify(globalConfig));
}

const autoSave = _.debounce(save, 2000);

const get = exports.get = function() {
    return globalConfig;
}

const set = exports.set = function(newConfig, doAutoSave) {
    if(doAutoSave === undefined) {
        doAutoSave = true;
    }

    globalConfig = globalConfig.merge(
        normalize(newConfig)
    );

    emitter.emit("changed", globalConfig);

    if(doAutoSave)
        autoSave();

    return globalConfig;
}

exports.on = function() {
    emitter.on.apply(emitter, arguments);
}

load();